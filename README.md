## Use SQLite Databases on Deta Drive (Deta.space)
The Deta.Space platform supports NoSql databases, which are more suitable for small projects, and this form of database has many problems that are not commensurate with the nature of the project I am working on, and for this I found a way to use Sqlite databases on the platform.

The code I provided defines a class named `DetaDriveDatabase` which is used to interact with SQLite database stored on Deta Drive. This class contains many methods that can be used to manipulate the database, such as creating new tables, inserting data, updating data, deleting data, and selecting data.

You can use the library with an external file by calling it like this

```python
from Deta_Drive_Database_api import DetaDriveDatabase
```
It should be a file `Deta_Drive_Database_api.py` Exists in the same project as yours.

To create a new instance of this class, you must provide the Deta Project Key, Deta Drive Name, and Database Name as follows:

```python
deta_project_key = "YOUR_DETA_PROJECT_KEY"
drive_name = "YOUR_DETA_DRIVE_NAME"
db_name = "YOUR_DATABASE_NAME.db"

db = DetaDriveDatabase(deta_project_key, drive_name, db_name)
```

After creating a new instance of the class, the `connect` method must be called to connect to the database:

```python
db. connect()
```

Once connected to the database, the various methods in the `DetaDriveDatabase` class can be used to manipulate the database. For example, to create a new table, the `create_table` method can be used as follows:

```python
table_name = "YOUR_TABLE_NAME"
columns = {"id": "INTEGER PRIMARY KEY AUTOINCREMENT", "name": "TEXT", "value": "INTEGER"}

db.create_table(table_name, columns)
```

Similarly, other methods such as `insert_data`, `update_data`, `delete_data`, and `select_data` can be used to insert, update, delete and select data in the database.

When you are finished using the database, the `close` method must be called to close the database connection:

```python
db. close()
```

### full usage example

```python
# First, create an instance of the DetaDriveDatabase class by providing your Deta project key, drive name and database name as arguments to its constructor
deta_project_key = "YOUR_DETA_PROJECT_KEY"
drive_name = "YOUR_DETA_DRIVE_NAME"
db_name = "YOUR_DATABASE_NAME.db"
db = DetaDriveDatabase(deta_project_key, drive_name, db_name)

# Connect to the database
db.connect()

# Create a new table
table_name = "items"
columns = {"id": "INTEGER PRIMARY KEY AUTOINCREMENT", "name": "TEXT", "value": "INTEGER"}
db.create_table(table_name, columns)

# Insert data into the database
table_name = "items"
columns = ["name", "value"]
values = [("apple", 10), ("banana", 20), ("orange", 30)]
db.insert_data(table_name, columns, values)

# Update some data in the database
set_values = {"value": 200}
where_condition = {"name": "apple"}
db.update_data(table_name, set_values, where_condition)

# Delete some data from the database
where_condition = {"name": "banana"}
db.delete_data(table_name, where_condition)

# Select some data from the database
result = db.select_data(table_name)
print(result)

# Rename the table
old_table_name = "items"
new_table_name = "products"
db.rename_table(old_table_name, new_table_name)

# Clear the table
table_name = "products"
db.clear_table(table_name)

# Drop the table
table_name = "products"
db.drop_table(table_name)

# Save changes to the database
db.save()

# Delete the database
db.delete_database()

# Close the database connection and save
db.close()
```

### Documentation for each method in the `DetaDriveDatabase` class and how to use them:

- `__init__(self, deta_project_key, drive_name, db_name)`: This method initializes the `DetaDriveDatabase` object. It takes in a Deta project key, drive name and database name as arguments and initializes the Deta library with the provided project key. It also gets a reference to the specified Deta Drive and stores the database name for later use.

- `connect(self)`: This method checks if the database file exists on Deta Drive and creates a new empty in-memory SQLite database if it doesn't exist. It then reads the database file from Deta Drive as bytes and creates an in-memory SQLite database.

- `close(self)`: This method dumps the contents of the in-memory database to a new `BytesIO` object and uploads the updated database file to Deta Drive. It then closes the database connection and sets it to `None`.

- `save(self)`: This method checks if the database is connected and raises an exception if it's not. It then dumps the contents of the in-memory database to a new `BytesIO` object and uploads the updated database file to Deta Drive.

- `load_db_to_memory(self, dbapi_con)`: This method loads the database from Deta Drive into memory. It takes in a dbapi_con argument which is a database connection object. It then gets the database file from Deta Drive and checks if it exists. If it does, it reads the file as bytes and executes the script in the in-memory database.

- `save_memory_to_drive(self, dbapi_con)`: This method saves the in-memory database to Deta Drive. It takes in a dbapi_con argument which is a database connection object. It then iterates over the contents of the in-memory database and dumps them to a string. It then encodes the string as bytes and saves the file to Deta Drive.

- `delete_database(self)`: This method checks if the database is connected and raises an exception if it's not. It then closes the database connection and sets it to `None`. Finally, it deletes the database file from Deta Drive.

- `clear_table(self, table_name)`: This method checks if the database is connected and raises an exception if it's not. It then executes a `DELETE FROM` SQL statement to delete all rows from the specified table.

- `clear_database(self)`: This method checks if the database is connected and raises an exception if it's not. It then gets a list of all tables in the database and truncates each table.

- `create_table(self, table_name, columns)`: This method checks if the database is connected and raises an exception if it's not. It then creates a string representation of the columns and their data types and executes a `CREATE TABLE` SQL statement.

- `drop_table(self, table_name)`: This method checks if the database is connected and raises an exception if it's not. It then executes a `DROP TABLE` SQL statement.

- `rename_table(self, old_table_name, new_table_name)`: This method checks if the database is connected and raises an exception if it's not. It then executes an `ALTER TABLE RENAME TO` SQL statement.

- `insert_data(self, table_name, columns, values)`: This method checks if the database is connected and raises an exception if it's not. It then creates a string representation of the columns and placeholders for the values and executes an `INSERT INTO` SQL statement for each set of values.

- `update_data(self, table_name, set_values, where_condition)`: This method checks if the database is connected and raises an exception if it's not. It then creates a string representation of the `SET` clause and the `WHERE` clause and combines the values from the `SET` and `WHERE` clauses into a single list. It then executes an `UPDATE` SQL statement.

- `delete_data(self, table_name, where_condition)`: This method checks if the database is connected and raises an exception if it's not. It then creates a string representation of the `WHERE` clause and executes a `DELETE FROM` SQL statement.

- `select_data(self, table_name, columns="*", where_condition=None)`: This method checks if the database is connected and raises an exception if it's not. It then creates a string representation of the columns to select and executes a `SELECT` SQL statement with or without a `WHERE` clause depending on whether a `where_condition` is provided. It then returns all rows from the `SELECT` statement.

To use these methods, you need to create an instance of the `DetaDriveDatabase` class by providing your Deta project key, drive name and database name as arguments to its constructor. You can then call its methods on this instance to interact with your Deta Drive-based SQLite database.

### How to use it with SQLAlchemy:

database.py:
```python
from sqlalchemy import create_engine, event
from sqlalchemy.orm import sessionmaker
from Deta_Drive_Database_api import DetaDriveDatabase
from sqlalchemy.ext.declarative import declarative_base


deta_project_key = "YOUR_DETA_PROJECT_KEY"
drive_name = "YOUR_DETA_DRIVE_NAME"
db_name = "YOUR_DATABASE_NAME.db"

db_deta = DetaDriveDatabase(deta_project_key, drive_name, db_name)

engine = create_engine('sqlite:///:memory:', echo=False)

@event.listens_for(engine, 'connect')
def connect(dbapi_connection, connection_record):
    db_deta.load_db_to_memory(dbapi_connection)

@event.listens_for(engine, 'commit')
def commit(dbapi_connection):
    db_deta.save_memory_to_drive(dbapi_connection.connection)

SessionLocal = sessionmaker(bind=engine, autocommit=False, autoflush=False)
Base = declarative_base()

# with FastAPI Project
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

# with Normal Python Script
def get_db_normal():
    db = SessionLocal()
    return db

```

after add database.py file inside your project you can use it by :

```python
from database import get_db_normal
db = get_db_normal()

```
from **FastAPI** Project:

```python
from fastapi import APIRouter, Depends, HTTPException,status
from schemas import Departments as schemas_Departments
import oauth2
import models

from sqlalchemy.orm import Session
# import from database.py 
from database import get_db

router = APIRouter(
    prefix="/api/Departments",
    tags=['Departments'],
)

@router.get("/")
def Get(db: Session = Depends(get_db) ):
    Departments = db.query(models.Department).all()
    
    if not Departments:
        raise HTTPException(status.HTTP_404_NOT_FOUND,
                            detail="there is no data")
        
    return Departments

```